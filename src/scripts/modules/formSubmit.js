import $ from 'jquery';

const FORM_SELECTOR = '.js-form-submit';
const FIELD_CONT_SELECTOR = '.js-form-field-cont';
const FORM_ERROR_NOTE_SELECTOR = '.js-form-note-error';

const ERROR_CLASS = 'has-error';

const REQUIRED_ERROR_MSG = 'This field is required.';
const LETTERS_ERROR_MSG = 'Only letters available.';
const NUMBER_ERROR_MSG = 'Only numbers available.';
const EMAIL_ERROR_MSG = 'Use correct email format, e.g. example@example.com';
const MAXLENGTH_ERROR_MSG = 'Maximum number of characters is:';

const LETTERS_PATTERN = /^[A-Za-z]+$/;
const NUMBER_PATTERN = /^[0-9]*$/;
const EMAIL_PATTERN = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const cleareFormErrors = $form => {
  $form.find(`.${ERROR_CLASS}`).removeClass(ERROR_CLASS);
  $form.find(FORM_ERROR_NOTE_SELECTOR).remove();
};

const drawErrorMsg = msg =>
  `<div class="o-form__note is-error js-form-note-error">${msg}</div>`;

const renderFieldErrorMsg = ($field, msg) => {
  $field
    .addClass(ERROR_CLASS)
    .closest(FIELD_CONT_SELECTOR)
    .append(drawErrorMsg(msg));
};

const validateReuiredField = ($field, fieldValue) => {
  let validField = true;

  if (fieldValue.length === 0) {
    renderFieldErrorMsg($field, REQUIRED_ERROR_MSG);
    validField = false;
  }

  return validField;
};

const validateFieldType = ($field, userFieldValue) => {
  const FIELD_TYPE = $field.data('type');
  let fieldValue = userFieldValue;
  let validField = true;

  fieldValue = fieldValue.trim();

  if (fieldValue.length > 0) {
    switch (FIELD_TYPE) {
      case 'letters':
        if (!LETTERS_PATTERN.test(fieldValue)) {
          renderFieldErrorMsg($field, LETTERS_ERROR_MSG);
          validField = false;
        }

        break;
      case 'number':
        if (!NUMBER_PATTERN.test(fieldValue)) {
          renderFieldErrorMsg($field, NUMBER_ERROR_MSG);
          validField = false;
        }

        break;
      case 'email':
        if (!EMAIL_PATTERN.test(fieldValue)) {
          renderFieldErrorMsg($field, EMAIL_ERROR_MSG);
          validField = false;
        }
        break;
      default:
        break;
    }
  }

  return validField;
};

const validateFieldMaxlength = ($field, fieldValue) => {
  const FIELD_MAXLENGTH = $field.attr('maxlength');
  let validField = true;

  if (fieldValue.length > FIELD_MAXLENGTH) {
    renderFieldErrorMsg($field, `${MAXLENGTH_ERROR_MSG} ${FIELD_MAXLENGTH}`);
    validField = false;
  }

  return validField;
};

const validateForm = $form => {
  let validForm = true;

  cleareFormErrors($form);

  $form.find('input, textarea').each((index, field) => {
    const $FIELD = $(field);
    const FIELD_VAL = $FIELD.val();

    if (typeof $FIELD.data('required') !== 'undefined') {
      validForm = validateReuiredField($FIELD, FIELD_VAL) ? validForm : false;
    }

    if (typeof $FIELD.data('type') !== 'undefined') {
      validForm = validateFieldType($FIELD, FIELD_VAL) ? validForm : false;
    }

    if (typeof $FIELD.attr('maxlength') !== 'undefined') {
      validForm = validateFieldMaxlength($FIELD, FIELD_VAL) ? validForm : false;
    }
  });

  return validForm;
};

const proceedForm = () => {
  alert('SUBMIT FORM');
};

const submitForm = event => {
  event.preventDefault();

  const $FORM = $(event.currentTarget);
  const VALID_FORM = validateForm($FORM);

  if (VALID_FORM) {
    proceedForm();
  }

  return false;
};

const init = () => {
  $(document)
    .off('.submitForm')
    .on('submit.submitForm', FORM_SELECTOR, submitForm);
};

export default init;
