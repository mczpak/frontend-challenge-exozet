import $ from 'jquery';

const TOGGLE_SELECTOR = '.js-toggle';

const ACTIVE_CLASS = 'is-active';

const getTarget = $trigger => {
  const TARGET = $trigger.data('target');
  let $target;

  if (TARGET === '#next') {
    $target = $trigger.next();
  } else if (TARGET === '#prev') {
    $target = $trigger.prev();
  } else if (TARGET === '#parent') {
    $target = $trigger.parent();
  } else if ($(TARGET).length > 0) {
    $target = $(TARGET);
  }

  return $target;
};

const getClassToChange = $trigger => $trigger.data('class') || ACTIVE_CLASS;

const toggleClass = event => {
  const $TRIGGER = $(event.currentTarget);
  const CLASS_TO_CHANGE = getClassToChange($TRIGGER);
  const $TARGET = getTarget($TRIGGER);

  if (typeof $TARGET !== 'undefined') {
    $TARGET.toggleClass(CLASS_TO_CHANGE);
  }

  $TRIGGER.toggleClass(ACTIVE_CLASS);
};

const init = () => {
  $(document)
    .off('.changeClass')
    .on('click.changeClass', TOGGLE_SELECTOR, toggleClass);
};

export default init;
