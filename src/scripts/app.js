/*
  Project: Frontend Challange Exozet
  Author: Mateusz Czpak
 */

import changeClass from './modules/changeClass';
import formSubmit from './modules/formSubmit';

changeClass();
formSubmit();
